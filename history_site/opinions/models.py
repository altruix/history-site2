from django.db import models
from django.utils.translation import ugettext_lazy as _

class Topic(models.Model):
	title = models.CharField(max_length=140)
	visible = models.NullBooleanField(null=True, blank=True, default=False)

	def __unicode__(self):
		return self.title
	class Meta:
		verbose_name = _('topic')
		verbose_name_plural = _('topics')

class ArgumentElementType(models.Model):
	contents = models.CharField(max_length=100)

	def __unicode__(self):
		return self.contents

class ConnectionType(models.Model):
	contents = models.CharField(max_length=100)

	def __unicode__(self):
		return self.contents

class ArgumentElement(models.Model):
	contents = models.CharField(max_length=256)
	elementType = models.ForeignKey(ArgumentElementType, related_name='ArgumentElement_ArgumentElementType')
	topic = models.ForeignKey(Topic, related_name='ArgumentElement_Topic')
	url = models.CharField(max_length=1000, null=True, blank=True)

	def __unicode__(self):
		return self.contents

class ArgumentElementConnection(models.Model):
	sourceId = models.ForeignKey(ArgumentElement, related_name='connection_source')
	targetId = models.ForeignKey(ArgumentElement, related_name='connection_target')
	topic = models.ForeignKey(Topic, related_name='ArgumentElementConnection_Topic')
	connectionType = models.ForeignKey(ConnectionType, related_name='ArgumentElementConnection_ConnectionType')

	def __unicode__(self):
		return self.sourceId.contents + ' -> ' + self.targetId.contents

class TopicLabel(models.Model):
	name = models.CharField(max_length=256)
	order = models.IntegerField(null=True, blank=True)
	topics = models.ManyToManyField(Topic, through='TopicLabelConnection')
#	visible_topics = models.ManyToManyField(Topic, through='TopicLabelConnection').objects.filter(visible=True)

	def getVisibleTopics(self):
		return self.topics.filter(visible=True)
#		return topics.filter(visible=True)
#		return Topic.objects.filter(connection_label__visible=True).filter(connection_label__id=self.id)
#         return Topic.objects.filter(connection_label__visible=True).filter(connection_label__id=self.id)

	def __unicode__(self):
		return self.name

class TopicLabelConnection(models.Model):
	topicId = models.ForeignKey(Topic, related_name='connection_topic')
	labelId = models.ForeignKey(TopicLabel, related_name='connection_label')

	def __unicode__(self):
		return self.labelId.name + ' / ' + self.topicId.title
