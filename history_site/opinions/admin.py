from django.contrib import admin
from history_site.opinions.models import ArgumentElement, ArgumentElementConnection, ArgumentElementType, Topic, ConnectionType, TopicLabel, TopicLabelConnection

admin.site.register(ArgumentElementType, admin.ModelAdmin)
admin.site.register(ArgumentElement, admin.ModelAdmin)
admin.site.register(ArgumentElementConnection, admin.ModelAdmin)
admin.site.register(Topic, admin.ModelAdmin)
admin.site.register(ConnectionType, admin.ModelAdmin)
admin.site.register(TopicLabel, admin.ModelAdmin)
admin.site.register(TopicLabelConnection, admin.ModelAdmin)