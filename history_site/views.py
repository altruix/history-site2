from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import get_template
from django.template import Context, RequestContext
from django.views.decorators.csrf import csrf_protect
import logging
from opinions.models import Topic, ArgumentElement, ArgumentElementConnection, TopicLabel
from django.template.response import TemplateResponse
import pydot

logging.basicConfig(filename='history-site.log',level=logging.DEBUG)

def home(request):		
	#labels = TopicLabel.objects.filter(connection_label__isnull=False).distinct().order_by('order')
	#labels = TopicLabel.getLabelsWithVisibleTopics()
	labels = TopicLabel.objects.filter(connection_label__topicId__visible=True)
	return TemplateResponse(request, 'home.tpl.html', locals())

def topic_details(request, topic_id_string):
	topic_id = int(topic_id_string)
	topic = Topic.objects.get(id=topic_id)
	arg_elements = ArgumentElement.objects.filter(topic=topic)
	
	graph = pydot.Dot(graph_type='digraph')

	for cur_arg_element in arg_elements:
		cur_node = pydot.Node('node' + str(cur_arg_element.id), label=cur_arg_element.elementType.contents + '\n' + str(cur_arg_element.id) + ': ' + cur_arg_element.contents)
		graph.add_node(cur_node)
	
	arg_conns = ArgumentElementConnection.objects.filter(topic=topic)

	for cur_arg_conn in arg_conns:
		graph.add_edge(pydot.Edge('node' + str(cur_arg_conn.sourceId.id), 'node' + str(cur_arg_conn.targetId.id)))

	graph.write_png('media/img/argumentation-diagrams/diagram-' + topic_id_string + '.png')

	return TemplateResponse(request, 'topic.tpl.html', locals())